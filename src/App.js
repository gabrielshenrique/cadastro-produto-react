import React from "react";
import NavBar from "./components/shared/NavBar";
// import Home from "./components/views/Home";
import Rotas from "./Routes/index";

import { HashRouter } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <HashRouter>
        <div className="container">
          <NavBar />
          <Rotas />
        </div>
      </HashRouter>
    </div>
  );
}

export default App;
