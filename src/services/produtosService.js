/*
API fake
*/

export function errorValidacoa(erros) {
  this.erros = erros;
}

export default class ProdutoService {
  validar = (produto) => {
    let erros = [];

    if (!produto.nome) {
      erros.push("O campo nome é obrigatorio");
    }

    if (!produto.sku) {
      erros.push("O campo nome sku é obrigátorio");
    }

    if (!produto.preco || produto.preco <= 0) {
      erros.push("O campo nome preço deve ser maior que (0)");
    }

    if (!produto.fornecedor) {
      erros.push("O campo fornecedor é obrigátorio");
    }

    if (erros.length) {
      throw new errorValidacoa(erros);
    }
  };

  obterIndex = (sku) => {
    let indexAtual = null;
    this.obterProdutos().forEach((element, index) => {
      if (element.sku === sku) {
        indexAtual = index;
      }
    });

    return indexAtual;
  };

  salvar = (produto) => {
    this.validar(produto);
    let produtos = sessionStorage.getItem("_PRODUTOS");
    if (!produtos) {
      produtos = [];
    } else {
      produtos = JSON.parse(produtos);
    }

    let index = this.obterIndex(produto.sku);

    if (index === null) {
      produtos.push(produto);
    } else {
      produtos[index] = produto;
    }

    sessionStorage.setItem("_PRODUTOS", JSON.stringify(produtos));
  };

  obterProdutos = () => {
    let prods = sessionStorage.getItem("_PRODUTOS");

    if (!prods) {
      return [];
    }
    return JSON.parse(prods);
  };

  deletar = (sku) => {
    let index = this.obterIndex(sku);
    if (index !== null) {
      let produtos = this.obterProdutos();
      produtos.splice(index, 1);
      sessionStorage.setItem("_PRODUTOS", JSON.stringify(produtos));
      return produtos;
    }
  };
}
