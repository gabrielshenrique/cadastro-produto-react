import React from "react";

import { Link } from "react-router-dom";


function Home() {
  return (
    <div className="home">
      <div className="jumbotron">
        <h1 className="display-3">Bem vindo!</h1>
        <p className="lead">
          Sistema de cadastro
        </p>
        <hr className="my-4" />
        <p>
          Utilize a navegação para aceassar as páginas.
        </p>
        <p className="lead">
          <Link className="btn btn-primary btn-lg" to="/produtos/cadastro" role="button">
            Cadastrar
          </Link>
        </p>
      </div>
    </div>
  );
}

export default Home;
