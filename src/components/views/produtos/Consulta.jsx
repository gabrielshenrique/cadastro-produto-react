import React from "react";

import ProdutoService from "../../../services/produtosService";

import { withRouter } from "react-router-dom";

import Card from "../../shared/Cards";

class ConsultaProdutos extends React.Component {
  state = {
    produtos: [],
  };

  constructor() {
    super();
    this.service = new ProdutoService();
  }

  componentDidMount() {
    let produtos = this.service.obterProdutos();
    //   this.setState({produtos: produtos})

    this.setState({ produtos });
  }

  preparaEditar = (sku) => {
    this.props.history.push(`/produtos/cadastro/${sku}`);
  };

  deletaProduto = (sku) => {
    let produtos = this.service.deletar(sku);
    this.setState({ produtos });
  };

  render() {
    return (
      <Card header="Listagem de Produtos">
        <table className="table table-hover">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Sku</th>
              <th>Preco</th>
              <th>Fornecedor</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.produtos.map((p, index) => {
              return (
                <tr key={index}>
                  <td>{p.nome}</td>
                  <td>{p.sku}</td>
                  <td>{p.preco}</td>
                  <td>{p.fornecedor}</td>
                  <td>
                    <button
                      onClick={() => this.preparaEditar(p.sku)}
                      className="btn btn-primary"
                    >
                      Editar
                    </button>
                    <span> </span>
                    <button
                      onClick={() => this.deletaProduto(p.sku)}
                      className="btn btn-danger"
                    >
                      Remover
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </Card>
    );
  }
}

export default withRouter(ConsultaProdutos);
