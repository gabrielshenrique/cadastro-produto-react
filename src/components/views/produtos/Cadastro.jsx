import React from "react";
import ProdutoService from "../../../services/produtosService";
import { withRouter } from "react-router-dom";
import Card from "../../shared/Cards";

class Produtos extends React.Component {
  state = {
    nome: "",
    sku: "",
    descricao: "",
    preco: 0,
    fornecedor: "",
    isSucesso: false,
    isErro: [],
    atualizando: false,
  };

  constructor() {
    super();
    this.service = new ProdutoService();
  }

  onChange = (event) => {
    let valor = event.target.value;
    let nomeCampo = event.target.name;
    this.setState({ [nomeCampo]: valor });
  };

  onSubmit = (e) => {
    e.preventDefault();
    let produto = {
      nome: this.state.nome,
      sku: this.state.sku,
      descricao: this.state.descricao,
      preco: this.state.preco,
      fornecedor: this.state.fornecedor,
    };

    try {
      this.service.salvar(produto);
      this.setState({ isSucesso: true });
    } catch (error) {
      let errors = error.erros;
      this.setState({ isErro: errors });
    }
  };

  onClear = () => {
    this.setState({
      nome: "",
      sku: "",
      descricao: "",
      preco: 0,
      fornecedor: "",
    });
  };

  componentDidMount() {
    let sku = this.props.match.params.sku;
    if (sku) {
      let resultado = this.service.obterProdutos().filter((a) => a.sku === sku);
      if (resultado.length === 1) {
        let produtoAtual = resultado[0];
        this.setState({ ...produtoAtual, atualizando: true });
      }
    }
  }

  render() {
    return (
      <Card
        header={
          this.state.atualizando
            ? "Atualizando de Produto"
            : "Cadastro de Produto"
        }
      >
        {this.state.isSucesso && (
          <div className="alert alert-dismissible alert-success">
            <button type="button" className="close" data-dismiss="alert">
              &times;
            </button>
            Cadastro realizado com sucesso
          </div>
        )}

        {this.state.isErro.length > 0 &&
          this.state.isErro.map((msg) => {
            return (
              <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">
                  &times;
                </button>
                {msg}
              </div>
            );
          })}

        <form onSubmit={this.onSubmit}>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label>Nome *</label>
                <input
                  type="text"
                  name="nome"
                  value={this.state.nome}
                  onChange={this.onChange}
                  className="form-control"
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label>SKU *</label>
                <input
                  type="text"
                  name="sku"
                  disabled={this.state.atualizando}
                  value={this.state.sku}
                  onChange={this.onChange}
                  className="form-control"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="form-group">
                <label>Descrição *</label>
                <textarea
                  name="descricao"
                  value={this.state.descricao}
                  onChange={this.onChange}
                  className="form-control"
                ></textarea>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label>Fornecedor *</label>
                <input
                  type="text"
                  name="fornecedor"
                  value={this.state.fornecedor}
                  onChange={this.onChange}
                  className="form-control"
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label>Preço *</label>
                <input
                  type="text"
                  name="preco"
                  value={this.state.preco}
                  onChange={this.onChange}
                  className="form-control"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-1">
              <button type="submit" className="btn btn-success">
                {this.state.atualizando ? "Atualizar " : "Salvar "}
              </button>
            </div>

            <div className="col-md-1">
              <button onClick={this.onClear} className="btn btn-info">
                Limpar
              </button>
            </div>
          </div>
        </form>
      </Card>
    );
  }
}

export default withRouter(Produtos);
