import React from "react";

import { Switch, Route } from "react-router-dom";

import Home from "../components/views/Home";

import CadastroProduto from "../components/views/produtos/Cadastro";

import ConsultaProduto from "../components/views/produtos/Consulta";

export default () => {
  return (
    
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/produtos/cadastro/:sku?" component={CadastroProduto} />
        <Route exact path="/produtos" component={ConsultaProduto} />
      </Switch>
    
  );
};
